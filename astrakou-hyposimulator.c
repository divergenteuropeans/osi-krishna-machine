// ************************************************************
// File: hypo-simulator.c
//
// Task Description:
// Your simulator program must follow the structure specified in the hypo machine document 
// and contain the functions (also called methods) specified in the simulator.
// Program should be well documented at (1) program level, (2) function level and (3) in-line.
//
// Author: Leanid Astrakou
// Date: Feb 18 2019
// Class: CSCI.465
// ************************************************************

#include <stdio.h>

// ************************************************************
// Function: InitializeSystem
//
// Task Description:
// 	Set all global system hardware components to 0 
//
// Input/Output/Function return
//	None
// ************************************************************

// The following initializes the constants

#define MEMSIZE                     10000
#define genPRSIZE                   8
#define OK                          0
#define HALT                        1
#define MAXRANGE                    3000
#define TIMESLICEEXPIRED		    2
#define INPUTOPERATION				3
#define OUTPUTOPERATION				4

#define STACKSIZE                   10
#define TIMESLICE                   200
#define OSMODE                      1
#define USERMODE                    2

#define ENDOFPROGRAM                -1
#define ENDOFLIST                   -1
#define ERRORINVALIDADDR            -2
#define ERRORINVALIDMODE            -3
#define ERRORINVALIDOPROGCTCODE     -4
#define ERRORINVALIDPROGCTVALUE     -5
#define ERRORFILEOPEN               -6
#define ERRORINVALIDADDRESS         -7
#define ERRORINVALIDGPR             -8
#define ERRORSTACKOVERFLOW          -9
#define ERRORSTACKUNDERFLOW         -10
#define ERRORNOFREEMEMORY           -11
#define ERRORINVALIDMEMORYSIZE      -12
#define ERRORINVALIDMEMORYORSIZE    -13

long mainMem[MEMSIZE]; // Main memory array
long memAddr; // Memory address register.
long memBffr; // Memory buffer register.
long sysClk; // System Clock.
long genPR[genPRSIZE]; // General purpose registers.
long instrR; // Instruction register.
long procR; // Process Status register
long progCt; // Program counter
long stackPt; // Stack pointer
long clock; // Clock
long readyQ; // Ready queue
long waitingQ; // Waiting queue
long userFreeList; // User Free List
long osFreeList; // OS Free List
long processID; // Process ID #
int shutdown; // Will be used to shut down program via interrupt

// Method prototypes
void initializeSystem();		
int absoluteLoader (char *);	
long fetchOperand(long , long, long *, long *); 
long CPU(); 
void dumpMemory(char*, long , long); 

  // HW2...
  long ioGetCSystemCall();
  long ioPutCSystemCall();
  long memFreeSystemCall();
  long memAllocSystemCall();
  long systemCall(long systemCallID);
  long createProcess(char *fileName, long priority);
  long allocateOSMemory(long requestedSize);
  long freeOSMemory(long ptr, long size);
  long allocateUserMemory(long requestedSize);
  long freeUserMemory(long ptr, long size);
  long searchAndRemovePCBFromWQ(long pid);
  long insertIntoRQ(long pcbPtr);

// PCB layout
// The following initializes variables based on an array & index order specified in the documentation
const int nextPCBPtr                = 0;
const int pcbPID                    = 1;
const int pcbState                  = 2;
const int pcbReason                 = 3;
const int pcbPriority               = 4;
const int pcbStackStartAddr         = 5;
const int pcbStackSize              = 6;
const int pcbGPR0                   = 11;
const int pcbGPR1                   = 12;
const int pcbGPR2                   = 13;
const int pcbGPR3                   = 14;
const int pcbGPR4                   = 15;
const int pcbGPR5                   = 16;
const int pcbGPR6                   = 17;
const int pcbGPR7                   = 18;
const int pcbSP                     = 19;
const int pcbPC                     = 20;
const int pcbPSR                    = 21;
const int pcbSize                   = 22;

// PCB values
const int defaultPriority           = 127;
const int readyState                = 1;
const int runningState              = 2;
const int waitingState              = 3;


void initializeSystem()
{
  printf("Initializing Hypo hardware components... \n");
  memAddr = 0;
  memBffr = 0;
  sysClk = 0;
  instrR = 0;
  procR = 0;
  progCt = 0;
  stackPt = 0;
  int fill = 0;
  while (fill < MEMSIZE)
  {
      mainMem[fill] = 0;
      fill++;
  }
  fill = 0;
  while (fill < genPRSIZE)
  {
      genPR[fill] = 0;
      fill++;
  }

  // HW2 ...
  userFreeList = 3000;
  mainMem[userFreeList] = ENDOFLIST;
  mainMem[userFreeList + 1] = 5000;

  osFreeList = 7000;
  mainMem[osFreeList] = ENDOFLIST;
  mainMem[osFreeList + 1] = 3000;
	
  readyQ = ENDOFLIST;
  waitingQ = ENDOFLIST;
	
  createProcess("p4hw2.txt", 0);
}

int main()
{
  char fileName[50]; // Intialize variables
  long completionStatus;
  long returnValue;
  initializeSystem(); // Call initialization method for globals

  printf("Enter file name: ");
  scanf("%s", fileName); // Get user input for file name
  printf("You entered: [%s]\n", fileName);
  returnValue = absoluteLoader(fileName);

  if (returnValue < -1) // End of program is -1
  {
    printf("Error Code(?): %ld\n", returnValue );
  }
  else
  {
    progCt = returnValue;
  }
  printf("progCt: %ld\n", progCt);
  dumpMemory("\n\nOutput after load -->", 0, 30); // Range from {10 - 30} from the doc specifications
  completionStatus = CPU();
  dumpMemory("Output after execute -->", 0, 30); // Range from {10 - 30} from the doc specifications

  return completionStatus;
}

// ************************************************************
// Function: AbsoluteLoader
//
// Task Description:
// 	Open the file containing HYPO machine user program and 
//	load the content into HYPO memory.
//	On successful load, return the progCt value in the End of Program line.
//	On failure, display appropriate error message and return appropriate error code
//
// Input Parameters
//	fileName			Name of the Hypo Machine executable file
//
// Output Parameters
//	None
//
// Function Return Value will be one of the following:
//	ERRORFILEOPEN			    Unable to open the file
//	ERRORINVALIDADDRESS		    Invalid address error
//	ERRORINVALIDPROGCTVALUE		Invalid progCt value
//	0 to Valid address range	Successful Load, valid progCt value
// ************************************************************
int absoluteLoader ( char * fileName)
{
  printf("Starting absoluteLoader...\n");
  
  // Open the file
  FILE *filePt = fopen(fileName,"r");

  if (filePt == NULL)
  {
    return (ERRORFILEOPEN); // If file opening failed...
  }

  int i = 0;
  long address;
  long content;

  while (1)
  {
    // Get the next address and content
    fscanf(filePt,"%ld", &address);
    fscanf(filePt,"%ld", &content);

    if (address == ENDOFPROGRAM) // Check if the end of the program has been reached
    {
      if (content >= 0 && content < MAXRANGE) // Check if the memory range is valid
      {
        return content; // Program counter variable
      }
      else // If range is invalid...
      {
          return (ERRORINVALIDPROGCTVALUE);
      }
    }

    else if (address >= 0 && content < MAXRANGE) // If it is not the end of the program
    {
      mainMem[address]= content;
    }

    else // If address is invalid
    {
      return (ERRORINVALIDADDRESS);
    }
  }

}

// ************************************************************
// Function: CPU
//
// Task Description:
// 	 Fetches the operands, executes, and increments the system clock.
//
//  Input/Output: none
//
//  Return: OK
//  Otherwise, Error code
// ************************************************************

long CPU()
{
  printf("In the CPU: \n");
  sysClk = 0; // System clock begins
  long OpCode;
  long rem; // OpCode remainder
  long Op1Mode;
  long Op1Gpr;
  long Op2Mode;
  long Op2Gpr;
  long Op1Address;
  long Op1Value;
  long Op2Address;
  long Op2Value;
  long stat = 0; // For status & errors
  long val;
  printf(" memBffr [%ld]\n", memBffr);
  printf(" progCt [%ld]\n", progCt);
  printf(" mainMem at progCt [%ld]\n", mainMem[progCt]);

  
  memBffr = ERRORINVALIDADDR;
  while (memBffr != 0) 
  {
    // FETCH
    if (progCt >= 0 && progCt <= MEMSIZE)
    {
      memAddr = progCt; // Make Address register equal to the program counter
      progCt = progCt + 1;
      memBffr = mainMem[memAddr]; // Read Hypo memory content pointed by memAddr into memBffr

    }
    else
    {
      return ERRORINVALIDPROGCTVALUE;
    }

    instrR = memBffr; // Copy memBffr value into instruction register instrR;

    OpCode= instrR / 10000; // Get Opcode
    rem = instrR % 10000;
    
    Op1Mode= rem / 1000; // Get O1pmode
    rem = rem % 1000;
    
    Op1Gpr= rem / 100; // Get op1 genPR
    rem = rem % 100;
    
    Op2Mode= rem / 10; // Get op2Mode
    rem = rem % 10;
    
    Op2Gpr = rem; // Op2 GPR

    if (Op1Gpr < 0 || Op1Gpr > 7 || Op2Gpr < 0 || Op1Gpr > 7) // If the Operand GPRS are out of range
    {
      return (ERRORINVALIDGPR);
    }
    else if (Op1Mode < 0 || Op1Mode > 6 || Op2Mode < 0 || Op2Mode > 6) // If the mode are out of range
    {
      return (ERRORINVALIDMODE);
    }  
    else if (OpCode < 0 || OpCode > 12) // If the Opcode is out of range
    {
      return (ERRORINVALIDOPROGCTCODE);
    }
    // No errors found, start execution cycle
    else
    {
      switch(OpCode)
      {
         case 0:
            sysClk = sysClk +12;
            break;
         case 1: stat = fetchOperand(Op1Mode, Op1Gpr,&Op1Address, &Op1Value); // Addition
        
            if (stat < 0)
            {
              return (stat);
            }
            stat = fetchOperand(Op2Mode, Op2Gpr, &Op2Address, &Op2Value);
            if (stat < 0)
            {
              return (stat);
            }
            // Add the operands
            Op1Value = Op1Value+Op2Value;
            if (Op1Mode == 1) // Check if it is register mode
            {
              genPR[Op1Gpr]=Op1Value; // Store Op1value into the genPR
            }
            else if (Op1Mode == 6)// In case it is in immediate mode
            {
              return (ERRORINVALIDMODE);
            }
            else // For everything else...
            {
              mainMem[Op1Address] = Op1Value; // Store the value into main memory array
            }
            sysClk = sysClk + 3;
            break;
            
        case 2: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Subtract
            
            if (stat < 0)
            {
                return (stat);
            }
            stat = fetchOperand(Op2Mode, Op2Gpr, &Op2Address, &Op2Value);
            if (stat < 0)
            {
                return (stat);
            }

            Op1Value = Op1Value-Op2Value;
            if (Op1Mode == 1) // If in register mode
            {
                genPR[Op1Gpr]= Op1Value; // Put op1value into GPR
            }         

            else if (Op1Mode == 6) // If in immediate mode
            {
                return (ERRORINVALIDMODE);
            }
            // For everything else...
            else
            {
                mainMem[Op1Address] = Op1Value;
            }
            sysClk = sysClk +3;
            break;

        case 3: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Multiply
            
            if (stat < 0)
            {
                return (stat);
            }
            stat = fetchOperand(Op2Mode, Op2Gpr, &Op2Address, &Op2Value);
            if (stat < 0)
            {
                return (stat);
            }

            Op1Value = Op1Value*Op2Value;
            if (Op1Mode == 1)
            {
                genPR[Op1Gpr]= Op1Value; // Put op1value into GPR
            }         

            else if (Op1Mode == 6) // If immediate
            {
                return (ERRORINVALIDMODE);
            }

            // For everything else...
            else
            {
            mainMem[Op1Address] = Op1Value;
            }
            sysClk = sysClk +6;
            break;
            
        case 4: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Divide
            
            if (stat < 0)
            {
            return (stat);
            }
            stat = fetchOperand(Op2Mode, Op2Gpr, &Op2Address, &Op2Value);
            if (stat < 0)
            {
            return (stat);
            }

            Op1Value = Op1Value/Op2Value;
            // Register Mode
            if (Op1Mode == 1)
            {
            genPR[Op1Gpr]= Op1Value; // Put op1value into GPR
            }         

            else if (Op1Mode == 6) // Checking for immediate mode
            {
            return (ERRORINVALIDMODE); 
            }

            // For everything else...
            else
            {
            mainMem[Op1Address] = Op1Value;
            }
            sysClk = sysClk +6;
            break;
        
        case 5: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Move

            if (stat < 0)
            {
            return (stat);
            }
            stat = fetchOperand(Op2Mode, Op2Gpr, &Op2Address, &Op2Value);
            if (stat < 0)
            {
            return (stat);
            }

            Op1Value = Op2Value;
            // Register Mode
            if (Op1Mode == 1)
            {
            genPR[Op1Gpr]= Op1Value; // Put op1value into GPR
            }         

            else if (Op1Mode == 6) // Checking for immediate mode
            {
            return (ERRORINVALIDMODE); 
            }

            // For everything else...
            else
            {
            mainMem[Op1Address] = Op1Value;
            }
            sysClk = sysClk +2;
            break;

        case 6: // Branch
            
            if (progCt < MEMSIZE && progCt >= 0) // Checking progCt range
            {
                progCt = mainMem[(int)progCt];
            }
            else
            {
                return ERRORINVALIDPROGCTVALUE;
            }
            sysClk = sysClk + 2;
            break;

        case 7: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Branch on minus
            
            if (stat < 0)
            {
                return (stat);
            }
            if (Op1Value < 0)
            {
                if (progCt < MEMSIZE && progCt >= 0)
                {
                progCt = mainMem[progCt];
                }
                else
                {
                return ERRORINVALIDPROGCTVALUE;
                }
            }
            // If opcode is greater than or equal to 0
            else
            {
                progCt++;
            }
            sysClk = sysClk +4;
            break;
        case 8: stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value); // Branch on plus
            
            if (stat < 0)
            {
                return stat;
            }
            if (Op1Value>0)
            {
                if (progCt < MEMSIZE && progCt >= 0)
                {
                progCt = mainMem[progCt];
                }
                else
                {
                return ERRORINVALIDPROGCTVALUE;
                }
            }
            else
            {
                progCt++;
            }
                sysClk = sysClk + 4;
            break;
            
        case 9: // Branch on zero
                
            stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value);
            if (stat < 0)
            {
                return stat;
            }
            if (Op1Value == 0)
            {
                if (progCt < MEMSIZE && progCt >= 0)
                {
                progCt = mainMem[progCt];
                }
                else
                {
                return ERRORINVALIDPROGCTVALUE;
                }
            }
            else
            {
                progCt++;
            }
                sysClk = sysClk +4;
            break;

        case 10: // Push
                
            stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value);
            if (stat < 0)
            {
                return stat;
            }
            if (MEMSIZE < stackPt)
            {
                return ERRORSTACKOVERFLOW;
            }
                stackPt++;
                mainMem[stackPt]=Op1Value;
                printf("%ld has been added to the stack.\n",Op1Value );
                break;
                sysClk = sysClk + 2;
        case 11:  // Pop
            
            stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value);
            if (stat < 0)
            {
                return stat;
            }
            if (stackPt < 0)
            {
                return ERRORSTACKUNDERFLOW;
            }
            if (stackPt<= MEMSIZE && stackPt>0)
            {
                Op1Address = mainMem[stackPt];
                stackPt--;
            }
            printf("%ld was popped.\n",Op1Address);
                sysClk = sysClk +2;

            break;

        case 12: // System call
            
            stat = fetchOperand(Op1Mode,Op1Gpr, &Op1Address,&Op1Value);
            if (stat < 0)
            {
                return stat;
            }
            break;

            default: return ERRORINVALIDOPROGCTCODE; // In case something screwed up

    }
  }
}
return stat;

}

// ************************************************************
// Function: fetchOperand
//
// Task Description: Fetches operand based on 6 cases.
//
// Input Parameters
//	OpMode			Operand mode value
//	OpReg				Operand genPR value
// Output Parameters
//	OpAddress			Address of operand
//	OpValue			Operand value when mode and genPR are valid
//
// Function Return Value
//	OK				On successful fetch
//	Error Code (#) otherwise
// ************************************************************

long fetchOperand(long OpMode, long OpReg, long *OpAddress, long *OpValue)	// Operand value, output parameter
{
	switch(OpMode)
    {
    // Register Mode
    case 1: 
        *OpAddress = ERRORINVALIDADDR;// OpAdress set to an invalid value
        *OpValue = genPR[OpReg]; // OPValue set to GPR
        break;

    // Register Deferred mode
    case 2: 
        *OpAddress = genPR[OpReg];
        *OpValue = genPR[OpReg];
        break;
    case 3: // Autoincrement mode 
        *OpAddress = genPR[OpReg];
        if (*OpAddress>0)
        {
        *OpValue = mainMem[*OpAddress];
        }
        else
        {
        return (ERRORINVALIDADDRESS);
        }
        genPR[OpReg]++;
        break;

    // Autodecrement mode
    case 4: --genPR[OpReg];
        *OpAddress = genPR[OpReg];
        if (*OpAddress>0)
        {
        *OpValue = mainMem[*OpAddress];
        }
        else
        {
        return (ERRORINVALIDADDRESS);
        }
        break;
  // Direct Mode
  case 5: *OpAddress = mainMem[progCt];
        progCt++;
        if (*OpAddress >= 0)
        {
        *OpValue = mainMem[*OpAddress];
        }
        else
        {
        return (ERRORINVALIDADDRESS);
        }
        break;

  // Immediate Mode
  case 6:// Instruction contains operand value
    if (progCt < MEMSIZE)
    {
        *OpValue = mainMem[progCt];
        progCt = progCt + 1;
        break;
    }
    else
    {
        return ERRORINVALIDPROGCTVALUE;
    }
  default: return (ERRORINVALIDMODE);
}
return OK;
    // Return success (0)
}

// ************************************************************
// Function: dumpMemory
//
// Task Description:
//	Displays a string passed as one of the  input parameter.
// 	Displays content of genPRs, stackPt, progCt, procR, system Clock and
//	the content of specified memory locations in a specific format.
//
// Input Parameters
//	String				String to be displayed
//	startAddr			Start address of memory location
//	Size				Number of locations to dump
// Output Parameters
//	None			
//
// Function Return Value
//	None				
// ************************************************************
void dumpMemory(char *String, long startAddr, long size)
{
  // Display input parameter String;
  printf("%s\n",String);

  printf("genPRs:\tG0\tG1\tG2\tG3\tG4\tG5\tG6\tG7\tstackPt\tprogCt");
  printf("\n");
  printf("\t");
  for(int i = 0; i < 8;i++)
  {
    printf("%ld\t",genPR[i]);
  }

  printf("%ld\t",stackPt);
  printf("%ld\t",progCt);
  printf("\n");
  printf("Address:+0\t+1\t+2\t+3\t+4\t+5\t+6\t+7\t+8\t+9\n");
  long addr = startAddr;
  long endAddr = startAddr + size;
  while (addr < endAddr)
  {
    printf("%ld\t", addr);

    int counter = 0;
    while (counter < 10)
    {
      if (addr < endAddr)
      {
        printf("%ld\t", mainMem[addr]);
        addr++;
      }
      else
      {
        break;
      }
      counter++;
    }
    printf("\n"); // Formatting
    
  } 
  printf("\n");
  printf("System Clock = %ld\n", sysClk);// Display System Clock
  printf("Program Status Register = %ld\n", procR);// Display Status Register
  return;
}

//Unmodified
// ************************************************************
// Function: systemCall
//
// Task Description:
//	Redirects to a system call based on the system call ID.
//
// Input Parameters
//	long      System call ID
// Output Parameters
//	None			
//
// Function Return Value
//	long      Status				
// ************************************************************
long systemCall(long systemCallID)
{
    procR = OSMODE; // Set system mode to OS mode
    long status = OK;

    switch(systemCallID) // The following cases are outlined in documentation with -->
    {   // Create process
        case 1: printf("Create process system call has not been implemented yet.\n");
                break;
        // Delete process
        case 2: printf("Delete process system call has not been implemented yet.\n");
                break;
        // Process inquiry
        case 3: printf("Process inquiry system call has not been implemented yet.\n");
                break;
        // Memory allocate
        case 4: status = memAllocSystemCall();
                break;
        // Memory free up
        case 5: status = memFreeSystemCall();
                break;
        case 6: break;
        case 7: break;
        // io getc
        case 8: status = ioGetCSystemCall();
                break;
        // io putc
        case 9: status = ioPutCSystemCall();
                break;
        // For all other cases...
        default: printf("Invalid system call ID.\n");
                break;

        procR = USERMODE; // Set mode back to user mode

        return(status);
    }
}
// ************************************************************
// Function: memAllocSystemCall
//
// Task Description:
//	Allocates memory from the user free list and returns the 
//  status of the address or any errors that occurred.
//
// Function Return Value
//	long      Address/Error status		
// ************************************************************

long memAllocSystemCall()
{
    long size = genPR[2];

    if (size < 0 || size > 7000)
    {
      return ERRORINVALIDMEMORYSIZE;
    }

    if (size == 1)
    {
      size = 2; // Check
    }

    genPR[1] = allocateUserMemory(size);

    if(genPR[1] < 0)
    {
        genPR[0] = genPR[1]; // Set GPR0 to have return status
    }
    else
    {
        genPR[0] = OK;
    }

    printf("Memory allocation system call parameters: \n%d\n%d\n%d\n", genPR[0], genPR[1], genPR[2]);

    return(genPR[0]);
}
// ************************************************************
// Function: memFreeSystemCall
//
// Task Description:
//	Returns the dynamically allocated user memory to the user free
//  list. GPR1 has memory address and GPR2 has the memory size to be released.
//
// Function Return Value
//	long      status (GPR0)		
// ************************************************************

long memFreeSystemCall()
{
    long size = genPR[2];

    if (size < 0 || size > 7000)
    {
      return ERRORINVALIDMEMORYSIZE;
    }

    if (size == 1)
    {
      size = 2; // Check
    }

    genPR[0] = freeUserMemory(genPR[1], size);

    printf("Memory free system call parameters: \n%d\n%d\n%d\n", genPR[0], genPR[1], genPR[2]);

    return(genPR[0]);
}

long ioGetCSystemCall()
{
    printf("ioget_c System Call\n");
    return (long)INPUTOPERATION; // Returns start of input operation event code
}

long ioPutCSystemCall()
{
	  printf("ioput_c System Call\n");
    return (long)OUTPUTOPERATION; // Returns start of output operation event code
}

// ************************************************************
// Function: createProcess
//
// Task Description:
//	Creates a process with a given priority 
//
// Function Return Value
//	long      status (GPR0)		
// ************************************************************

long createProcess(char *fileName, long priority)
{
    long pcbPtr = allocateOSMemory(pcbSize);
    if(pcbPtr < 0)
    {
        return(pcbPtr);
    }

    initializePCB(pcbPtr);

    long value = absoluteLoader(fileName);
    if(value < 0)
    {
        return(value);
    }

    mainMem[pcbPtr + pcbPC] = value;

    long ptr = allocateUserMemory(STACKSIZE);
    if(ptr < 0)
    {
        freeOSMemory(pcbPtr, pcbSize);
        return(ptr);
    }

    mainMem[pcbPtr + pcbSP] = ptr + STACKSIZE;
    mainMem[pcbPtr + pcbStackStartAddr] = ptr;
    mainMem[pcbPtr + pcbStackSize] = STACKSIZE;
    mainMem[pcbPtr + pcbPriority] = priority;

    dumpMemory("Dumping program area.\n", 0, 200);
    printPCB(pcbPtr);

	insertIntoRQ(pcbPtr);
	
	return(OK);
}

// ************************************************************
// Function: allocateOSMemory
//
// Task Description:
//	Allocates memory for OS given a size.
//  Input: requestedSize
//
// Function Return Value
//	long      status	
// ************************************************************
long allocateOSMemory(long requestedSize)
{
    if(osFreeList == ENDOFLIST)
    {
        printf("Error(?): There is no free memory.\n");
        return(ERRORNOFREEMEMORY);
    }

    if(requestedSize < 0)
    {
        printf("Error(?): Invalid (negative) memory size.");
        return(ERRORINVALIDMEMORYSIZE);
    }

    if(requestedSize == 1)
    {
        requestedSize = 2; // Minimum allocated memory is 2 locations
    }

    long currPtr = osFreeList;
    long prevPtr = ENDOFLIST;
	

    while(currPtr != ENDOFLIST)
    {
        // Check each block in the link list until block with requested memory size is found
        if(mainMem[currPtr + 1] == requestedSize)
        {
            if(currPtr == osFreeList)
            {
                osFreeList = mainMem[currPtr];
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }

            else
            {
                mainMem[prevPtr] = mainMem[currPtr];
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }
        }

        else if(mainMem[currPtr + 1] > requestedSize)
        {
            if(currPtr == osFreeList)
            {
                mainMem[currPtr + requestedSize] = mainMem[currPtr];
                mainMem[currPtr + requestedSize + 1] = mainMem[currPtr + 1];
                osFreeList = currPtr + requestedSize;
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }

            else
            {
                mainMem[currPtr + requestedSize] = mainMem[currPtr];
                mainMem[currPtr + requestedSize + 1] = mainMem[currPtr + 1];
                mainMem[prevPtr] = currPtr + requestedSize;
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }
        }

        else
        {
            prevPtr = currPtr;
            currPtr = mainMem[currPtr];
        }
    }
    printf("Error(?): There is no free memory.\n");
    return(ERRORNOFREEMEMORY);

}

// ************************************************************
// Function: freeOSMemory
//
// Task Description:
//	Frees up OS memory given a pointer and size.
//  Input: pointer, size
//
// Function Return Value
//	long      status	
// ************************************************************
long freeOSMemory(long ptr, long size)
{
    if(ptr < 7000 || ptr > 9999)
    {
        printf("Error(?): Invalid memory address.\n");
        return(ERRORINVALIDADDRESS);
    }

    if(size == 1) // Check for minimum allocated size, which is 2 even if user asks for 1 location

    {
        size = 2;
    }

    else if(size < 1 || (ptr + size) > 9999)
    {
        printf("Error(?): Invalid size or memory address.\n");
        return(ERRORINVALIDMEMORYORSIZE);
    }

	osFreeList = 7000;
    mainMem[osFreeList] = ENDOFLIST;
    mainMem[osFreeList + 1] = 3000;
    
	return(OK);
}
// ************************************************************
// Function: allocateUserMemory
//
// Task Description:
//	Allocate user memory given a size.
//  Input: size
//
// Function Return Value
//	long      status	
// ************************************************************
long allocateUserMemory(long requestedSize)
{
    if(userFreeList == ENDOFLIST)
    {
        printf("Error(?): There is no free memory.\n");
        return(ERRORNOFREEMEMORY);
    }

    if(requestedSize < 0)
    {
        printf("Error(?): Invalid memory size.");
        return(ERRORINVALIDMEMORYSIZE);
    }

    if(requestedSize == 1)
    {
        requestedSize = 2; // Again, user check
    }

    long currPtr = userFreeList;
    long prevPtr = ENDOFLIST;

    while(currPtr != ENDOFLIST)
    {
        if(mainMem[currPtr + 1] == requestedSize)
        {
            if(currPtr == userFreeList)
            {
                userFreeList = mainMem[currPtr];
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }

            else
            {
                mainMem[prevPtr] = mainMem[currPtr];
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }
        }

        else if(mainMem[currPtr + 1] > requestedSize)
        {
            if(currPtr == userFreeList)
            {
                mainMem[currPtr + requestedSize] = mainMem[currPtr];
                mainMem[currPtr + requestedSize + 1] = mainMem[currPtr + 1];
                userFreeList = currPtr + requestedSize;
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }

            else
            {
                mainMem[currPtr + requestedSize] = mainMem[currPtr];
                mainMem[currPtr + requestedSize + 1] = mainMem[currPtr + 1];
                mainMem[prevPtr] = currPtr + requestedSize;
                mainMem[currPtr] = ENDOFLIST;
                return(currPtr);
            }
        }

        else
        {
            prevPtr = currPtr;
            currPtr = mainMem[currPtr];
        }
    }
}
// ************************************************************
// Function: freeUserMemory
//
// Task Description:
//	Frees up user memory given a pointer and size.
//  Input: pointer, size
//
// Function Return Value
//	long      status	
// ************************************************************
long freeUserMemory(long ptr, long size)
{
    if(ptr < 3000 || ptr > 6999)
    {
        printf("Error(?): Invalid memory address.\n");
        return(ERRORINVALIDADDRESS);
    }

    if(size == 1)
    {
        size = 2; // Check
    }

    else if(size < 1 || (ptr + size) > 6999)
    {
        printf("Error(?): Invalid size or memory address.\n");
        return(ERRORINVALIDMEMORYORSIZE);
    }

    userFreeList = 3000;
    mainMem[userFreeList] = ENDOFLIST;
    mainMem[userFreeList + 1] = 5000;
	
	return(OK);
}
// ************************************************************
// Function: checkAndProcessInterrupt()
//
// Task Description:
//	Asks user for interrupt ID and processes interrupt.
//
// Function Return Value
//	none	
// ************************************************************
void checkAndProcessInterrupt()
{
    printf("0 - No Interrupt\n 1 - Run Program\n 2 - Shutdown System\n 3 - Input Operation Completion\n 4 - Output Operation Completion\n"); // Format in interrupts here follows the format specified in the documentation

    int interruptID;
    printf("Enter your Interrupt ID: ");
    scanf("%d", &interruptID);

	long status;
    switch(interruptID)
    {
        case 0: 
            break;
        case 1: ISRRunProgramInterrupt();
            break;
		case 2: ISRShutdownSystem();
			break;
        case 3: ISRInputCompletionInterrupt();
            break;
        case 4: ISROutputCompletionInterrupt();
            break;
        default: printf("Invalid interrupt ID.\n");
            break;
    }
	
	return;
}

void ISRRunProgramInterrupt()
{
    char file[50];
	
	printf("Enter file name: ");
    scanf("%s", &file);
    createProcess(file, defaultPriority);
	
	return;
}

void ISRInputCompletionInterrupt()
{
    int foundInRQ;
    int pid;
    printf("Enter PID: ");
    scanf("%d", &pid);

    long pcbPtr = searchAndRemovePCBFromWQ(pid);
    if(pcbPtr == ENDOFLIST)
    {
        pcbPtr = readyQ;
        while(pcbPtr != ENDOFLIST)
        {
            if(mainMem[pcbPtr + pcbPID] == pid)
            {
                /* Read a character from standard input */
				char character[1];
				printf("Enter character: ");
				scanf("%s", character);

                /* Store the character in the GPR of the PCB */
				long i = character[0] - '0';
				mainMem[pcbPtr + pcbGPR7] = i;
            }
            else
            {
                pcbPtr = mainMem[pcbPtr + nextPCBPtr];
            }
        }

        if(!foundInRQ)
        {
            printf("No matching PID found in RQ.\n");
        }
    }
    else
    {
        /* Read a character from standard input */
		char character[1];
		printf("Enter character: ");
		scanf("%s", character);

        /* Store the character in the GPR of the PCB */
		long i = character[0] - '0';
		mainMem[pcbPtr + pcbGPR7] = i;

        mainMem[pcbPtr + pcbState] = readyState;
        insertIntoRQ(pcbPtr);
    }
	
	return;
}

void ISROutputCompletionInterrupt()
{
    int foundInRQ;
    int pid;
    printf("Enter PID: ");
    scanf("%d", &pid);

    long pcbPtr = searchAndRemovePCBFromWQ(pid);
    if(pcbPtr == ENDOFLIST)
    {
        pcbPtr = readyQ;
        while(pcbPtr != ENDOFLIST)
        {
            if(mainMem[pcbPtr + pcbPID] == pid)
            {
				/* Print character stored in GPR of the pcb */
				char c = mainMem[pcbPtr + pcbGPR7] + '0';
                printf("Char is: %c\n", c);

            }
            else
            {
                pcbPtr = mainMem[pcbPtr + nextPCBPtr];
            }
        }

        if(!foundInRQ)
        {
            printf("No matching PID found in RQ.\n");
        }
    }
    else
    {
        /* Print character stored in GPR of the pcb */
		char c = mainMem[pcbPtr + pcbGPR7] + '0';
        printf("Char is: %c\n", c);

        mainMem[pcbPtr + pcbState] = readyState;
        insertIntoRQ(pcbPtr);
    }
	
	return;
}

void ISRShutdownSystem()
{
	shutdown = 1;
    long ptr;

    ptr = readyQ;
    while(ptr != ENDOFLIST)
    {
        readyQ = mainMem[readyQ + nextPCBPtr];
        terminateProcess(ptr);
        ptr = readyQ;
    }

    ptr = waitingQ;
    while(ptr != ENDOFLIST)
    {
        waitingQ = mainMem[waitingQ + nextPCBPtr];
        terminateProcess(ptr);
        ptr = waitingQ;
    }
	
	return;
}


// ************************************************************
// Function: initializePCB
//
// Task Description:
//	Initializes the memory array with the PCB info.
//  Input: pointer
//
// Function Return Value
//	long      status	
// ************************************************************
void initializePCB(long pcbPtr)
{
    int i;
    for(i = 0; i < pcbSize; i++)
    {
        mainMem[pcbPtr + i] = 0;
    }

    mainMem[pcbPtr + pcbPID] = processID++;
    mainMem[pcbPtr + pcbPriority] = defaultPriority;
    mainMem[pcbPtr + pcbState] = readyState;
    mainMem[pcbPtr + nextPCBPtr] = ENDOFLIST;
	
	return;
}
// ************************************************************
// Function: printPCB
//
// Task Description:
//	Prints PCB info given a pointer.
//  Input: pointer
//
// Function Return Value
//	none	
// ************************************************************
void printPCB(long pcbPtr)
{
    printf("PCB address = %d\n", pcbPtr);
    printf("Next PCB pointer = %d\n", mainMem[pcbPtr + nextPCBPtr]);
    printf("PID = %d\n", mainMem[pcbPtr + pcbPID]);
    printf("State = %d\n", mainMem[pcbPtr + pcbState]);
    printf("PC = %d\n", mainMem[pcbPtr + pcbPC]);
    printf("SP = %d\n", mainMem[pcbPtr + pcbSP]);
    printf("Priority = %d\n", mainMem[pcbPtr + pcbPriority]);
    printf("Stack Info: Start Address = %d\n", mainMem[pcbPtr + pcbStackStartAddr]);
    printf("Size = %d\n", mainMem[pcbPtr + pcbStackSize]);
    printf("GPR0 = %d, ", mainMem[pcbPtr + pcbGPR0]);
    printf("GPR1 = %d, ", mainMem[pcbPtr + pcbGPR1]);
    printf("GPR2 = %d, ", mainMem[pcbPtr + pcbGPR2]);
    printf("GPR3 = %d, ", mainMem[pcbPtr + pcbGPR3]);
    printf("GPR4 = %d, ", mainMem[pcbPtr + pcbGPR4]);
    printf("GPR5 = %d, ", mainMem[pcbPtr + pcbGPR5]);
    printf("GPR6 = %d, ", mainMem[pcbPtr + pcbGPR6]);
    printf("GPR7 = %d\n", mainMem[pcbPtr + pcbGPR7]);
	
	return;
}
// ************************************************************
// Function: terminateProcess
//
// Task Description:
//	Recover all resources allocated to the process.
//  Input: pcb pointer
//
// Function Return Value
//	none	
// ************************************************************
void terminateProcess(long pcbPtr)
{
    // Return stack memory using stack start address and stack size in the given PCB
    freeUserMemory(mainMem[pcbPtr + pcbStackStartAddr], mainMem[pcbPtr + pcbStackSize]);

    freeOSMemory(pcbPtr, pcbSize);
}
// ************************************************************
// Function: searchAndRemovePCBFromWQ
//
// Task Description:
//	Search the WQ for the matching pid.
//  When a match is found remove it from WQ and return PCB pointer.
//  Input: pcb pointer
//
// Function Return Value
//	ID status (if invalid, return invalid pid code)	
// ************************************************************
long searchAndRemovePCBFromWQ(long pid)
{
    long currentPCBPtr = waitingQ;
    long previousPCBPtr = ENDOFLIST;

    while(currentPCBPtr != ENDOFLIST)
    {
        if(mainMem[currentPCBPtr + pcbPID] == pid)
        {
            if(previousPCBPtr == ENDOFLIST)
            {
                waitingQ = mainMem[currentPCBPtr + nextPCBPtr];
            }
            else
            {
                mainMem[previousPCBPtr + nextPCBPtr] = mainMem[currentPCBPtr + nextPCBPtr];
            }
            mainMem[currentPCBPtr + nextPCBPtr] = ENDOFLIST;
            return(currentPCBPtr);
        }
        previousPCBPtr = currentPCBPtr;
        currentPCBPtr = mainMem[currentPCBPtr + nextPCBPtr];
    }

    printf("ERROR(?): No matching PID found in the waiting queue.\n");

    return(ENDOFLIST);
}
// ************************************************************
// Function: insertIntoRQ
//
// Task Description:
//   Insert PCB according to Priority Round Robin algorithm
//  Input: pcb pointer
//
// Function Return Value
//	status
// ************************************************************
long insertIntoRQ(long pcbPtr)
{
    long previousPtr = ENDOFLIST;
    long currentPtr = readyQ;

    if(pcbPtr < 7000 || pcbPtr > 9999)
    {
        printf("Error(?): Invalid memory address.\n");
        return(ERRORINVALIDADDRESS);
    }

    mainMem[pcbPtr + pcbState] = readyState;
    mainMem[pcbPtr + nextPCBPtr] = ENDOFLIST;

    if(readyQ == ENDOFLIST)
    {
        readyQ = pcbPtr;
        return(OK);
    }

    while(currentPtr != ENDOFLIST)
    {
       if(mainMem[pcbPtr + pcbPriority] > mainMem[currentPtr + pcbPriority])
       {
           if(previousPtr == ENDOFLIST)
           {
               mainMem[pcbPtr + nextPCBPtr] = readyQ;
               readyQ = pcbPtr;
               return(OK);
           }

           mainMem[pcbPtr + nextPCBPtr] = mainMem[previousPtr + nextPCBPtr];
           mainMem[previousPtr + nextPCBPtr] = pcbPtr;
           return(OK);
       }

       else
       {
           previousPtr = currentPtr;
           currentPtr = mainMem[currentPtr + nextPCBPtr];
       }
    }

    mainMem[previousPtr + nextPCBPtr] = pcbPtr;
    return(OK);
}
// ************************************************************
// Function: insertIntoWQ
//
// Task Description:
//   Insert the given PCB into waiting queue at the front of the queue.
//  Input: pcb pointer
//
// Function Return Value
//	status	
// ************************************************************
long insertIntoWQ(long pcbPtr)
{
    if(pcbPtr < 7000 || pcbPtr > 9999)
    {
        printf("Error(?): Invalid PCB address.\n");
        return(ERRORINVALIDADDRESS);
    }

    mainMem[pcbPtr + pcbState] = waitingState;
    mainMem[pcbPtr + nextPCBPtr] = waitingQ;

    waitingQ = pcbPtr;

    return(OK);
}

// ************************************************************
// Function: selectProcessFromRQ
//
// Task Description:
//   select the first process in the RQ and return the pointer to the PCB
//  Input: none
//
// Function Return Value
//	status	
// ************************************************************
long selectProcessFromRQ()
{
    long pcbPtr = readyQ;
    if(readyQ != ENDOFLIST)
    {
        readyQ = mainMem[readyQ + nextPCBPtr];
    }

    mainMem[pcbPtr + nextPCBPtr] = ENDOFLIST;

    return(pcbPtr);
}

// ************************************************************
// Function: saveContext
//
// Task Description:
//  Save CPU Context into Running Process PCB. CPU context consists of GPRs, SP, PC and PSR.
//  Input: pcb pointer
//
// Function Return Value
//	status	
// ************************************************************
void saveContext(long pcbPtr)
{
    mainMem[pcbPtr + pcbGPR0] = genPR[0];
    mainMem[pcbPtr + pcbGPR1] = genPR[1];
    mainMem[pcbPtr + pcbGPR2] = genPR[2];
    mainMem[pcbPtr + pcbGPR3] = genPR[3];
    mainMem[pcbPtr + pcbGPR4] = genPR[4];
    mainMem[pcbPtr + pcbGPR5] = genPR[5];
    mainMem[pcbPtr + pcbGPR6] = genPR[6];
    mainMem[pcbPtr + pcbGPR7] = genPR[7];

    mainMem[pcbPtr + pcbSP] = stackPt;
    mainMem[pcbPtr + pcbPC] = progCt;
}
// ************************************************************
// Function: dispatcher
//
// Task Description:
//  Restore the CPU context from the PCB into the CPU registers
//  Input: pcb pointer
//
// Function Return Value
//	status	
// ************************************************************
void dispatcher(long pcbPtr)
{
    genPR[0] = mainMem[pcbPtr + pcbGPR0];
    genPR[1] = mainMem[pcbPtr + pcbGPR1];
    genPR[2] = mainMem[pcbPtr + pcbGPR2];
    genPR[3] = mainMem[pcbPtr + pcbGPR3];
    genPR[4] = mainMem[pcbPtr + pcbGPR4];
    genPR[5] = mainMem[pcbPtr + pcbGPR5];
    genPR[6] = mainMem[pcbPtr + pcbGPR6];
    genPR[7] = mainMem[pcbPtr + pcbGPR7];

    stackPt = mainMem[pcbPtr + pcbSP];
    progCt = mainMem[pcbPtr + pcbPC];
    procR = USERMODE;
}
